﻿#pragma once

#include "processing.h"


void Analysis::Data_loading(Data& All_Frames, int exposure) {

	// Initializing Filenames for parallel data loading
	std::array<std::pair<int, std::string>, Available_Frames> Filenames;
	std::string parentFolderName = ParentFolderName;
	std::string folderName = "exp" + std::to_string(exposure) + FolderEndName;
	std::string fileInitial = FileInitial;
	std::string outFileName = parentFolderName + folderName;
	for (int Frame_Number = 0; Frame_Number < Available_Frames; Frame_Number++) {
		// Initliazation
		Filenames[Frame_Number].first = Frame_Number;
		Filenames[Frame_Number].second = parentFolderName + folderName + fileInitial + std::to_string(Frame_Number) + ".txt";

	}

	// Loading all frames to the data structure
	std::for_each(std::execution::par_unseq, Filenames.begin(), Filenames.end(), [&](std::pair<int, std::string>& Filename) {

		// Creating temporary data holder
		Frame_t* Frame = new Frame_t;

		// Loading Frame data from file
		Frame_Loading(*Frame, Filename.second);

		// Loading frame data in All_frames structure
		All_Frames.data_set[Filename.first] = *Frame;

		// Deleting old data
		delete Frame;
		});
}

void Analysis::Frame_Loading(Frame_t& Frame, std::string& Filename) {

	// Temp variable declaration
	std::string Pixel_Value;
	unsigned int i = 0;
	Pixel_t Pixel;

	// File stream object declaration
	std::ifstream Frame_File;

	// Opening file 
	Frame_File.open(Filename);

	// Check if file exists
	if (Frame_File.is_open()) {
		// Loading data from file
		while (Frame_File && i < Thorlabs_Frame_Resolution) {
			if (!std::getline(Frame_File, Pixel_Value, '\n')) break;
			if (Pixel_Value == "") break;
			Pixel = std::stoul(Pixel_Value);
			//if (Pixel == 0) continue;
			Frame[i] = Pixel;

			i++;
			//std::cout << i << std::endl;
		}
	}
	else std::cout << Filename << "doesn't exist" << std::endl;
	// Close file 
	Frame_File.close();

}

void Analysis::Raw_out() {

	// File stream object declaration
	std::string filename = Output_folder + this->outfile_name + std::to_string(this->Analysis_data.exposure_value) +
		this->Mode + "correlations" + std::to_string(Ref_Pixel) + ".csv";

	if (lag_status == true) filename = Output_folder + this->outfile_name + std::to_string(this->Analysis_data.exposure_value) +
		this->Mode + "correlations" + std::to_string(Ref_Pixel) + "lagged" + std::to_string(this->lag_value) + ".csv";
	std::ofstream Correlation_file; Correlation_file.open(filename);
	std::cout <<filename << std::endl;

	double avg = 0, sum = 0;
	// Check if file exists
	if (Correlation_file) {
#ifdef lag
		// Outputting correlation data
		int Col_Count = 0;
		for (auto correlation : this->correlation_array) {
			Col_Count = Col_Count + 1;
			if (lag_status == true) if (correlation == 0) continue;
			Correlation_file << correlation << ",";
			sum += correlation;
		}
#else
		// Outputting correlation data
		int Col_Count = 0;
		for (auto correlation : this->correlation_array) {
			Col_Count = Col_Count + 1;
			if (Col_Count >= 60) {
				Correlation_file << correlation << std::endl;
				Col_Count = 0;
			}
			else {
				Correlation_file << correlation << ",";
			}
			sum += correlation;
		}
#endif lag

		

		// Computing and outputting data min, max and average
		avg = sum / (this->correlation_array.size());
		Correlation_file << std::endl;
		Correlation_file << "max: " << *max_element(this->correlation_array.begin(), this->correlation_array.end()) << std::endl;
		Correlation_file << "min: " << *min_element(this->correlation_array.begin(), this->correlation_array.end()) << std::endl;
		Correlation_file << "avg: " << avg << std::endl;
	}

	// Close file 
	Correlation_file.close();
}

void Stats::Calculation() {
	
	// Initializing dummy vaiables
	long int sum = 0;
	double avg = 0, squareDiffrance = 0, var = 0;

	for (int i = 0; i < this->Analysis_data.data_set.size(); i++) {
		// Resetting the dummy variables
		sum = 0;
		avg = 0;
		var = 0;
		squareDiffrance = 0;

		// Calculating the average
		for (auto Pixel : this->Analysis_data.data_set[i]) {
			sum += Pixel;
		}
		avg = sum / double(this->Analysis_data.data_set[i].size());

		// Calculating the variance 
		for (auto Pixel : this->Analysis_data.data_set[i]) {
			squareDiffrance += (Pixel - avg) * (Pixel - avg);
		}
		var = squareDiffrance / this->Analysis_data.data_set[i].size();

		// Storing the stats
		this->Frame_avg_array[i] = avg;
		this->Frame_var_Array[i] = var;
	}

}

void Stats::Output() {

	
	// File stream object declaration
	std::ofstream Csv_File;
	std::string parentFolderName = ParentFolderName;
	std::string folderName = "exp" + std::to_string(this->Analysis_data.exposure_value) + FolderEndName;
	std::string outFileName = parentFolderName + folderName;

	// Defining the filename
	std::string filename;
	if (this->Analysis_data.BG_removed == 0) {
		filename = this->outfile_name + "Raw" + ".csv";
	}
	else
	{
		filename = this->outfile_name + "BGR" + ".csv";
	}

	// Initialize file if not initialized
	if (!std::filesystem::exists(filename)) {

		// Open stream file
		Csv_File.open(filename);

		// Print header
		Csv_File << "File Name" << "," << "Exposure" << "," << "Average" << "," << "," << "," << "Variance" << "," << "," << "," << "Fano Factor" << std::endl;
		Csv_File << "," << "," << "Min" << "," << "Max" << "," << "Avg" << "," << "Min" << "," << "Max" << "," << "Avg" << "," << "Min" << "," << "Max" << "," << "Avg" << std::endl;

	}
	else {
		// File stream object declaration
		Csv_File.open(filename, std::ios_base::app);
	}

	if (Csv_File) 	std::cout << this->Analysis_data.exposure_value << " stats are printed " << std::endl;

	// output file name and exposure
	Csv_File << outFileName << ",";
	Csv_File << this->Analysis_data.exposure_value << ",";

	// For The average
	double avg = 0, sum = 0, available_frames = Available_Frames;
	for (int i = 0; i < this->Frame_avg_array.size(); i++) {
		//std::cout << Avg_Array[i] << std::endl;
		sum += this->Frame_avg_array[i];

	}
	Csv_File << *min_element(this->Frame_avg_array.begin(), this->Frame_avg_array.end()) << ",";
	Csv_File << *max_element(this->Frame_avg_array.begin(), this->Frame_avg_array.end()) << ",";
	avg = sum / available_frames;
	//std::cout << avg_correlation << std::endl;
	Csv_File << avg << ",";


	// For variance
	avg, sum = 0;
	for (int i = 0; i < this->Frame_var_Array.size(); i++) {
		//std::cout << Var_Array[i] << std::endl;
		sum += this->Frame_var_Array[i];
		//std::cout << i << "   " << std::fixed << std::setprecision(8) << sum_correlation << std::endl;
	}
	Csv_File << *min_element(this->Frame_var_Array.begin(), this->Frame_var_Array.end()) << ",";
	Csv_File << *max_element(this->Frame_var_Array.begin(), this->Frame_var_Array.end()) << ",";
	avg = sum / available_frames;
	Csv_File << avg << ",";


	// Calculating the ratios
	avg = 0;
	sum = 0;
	std::array<double, Available_Frames> Fano_Factor;
	for (int i = 0; i < Fano_Factor.size(); i++) {
		Fano_Factor[i] = this->Frame_var_Array[i] / this->Frame_avg_array[i];
		sum += Fano_Factor[i];
	}
	Csv_File << *min_element(Fano_Factor.begin(), Fano_Factor.end()) << ",";
	Csv_File << *max_element(Fano_Factor.begin(), Fano_Factor.end()) << ",";
	avg = sum / available_frames;
	Csv_File << avg << std::endl;


	// Close file 
	Csv_File.close();
}

void SOCF::Output() {

	// File stream object declaration
	std::ofstream Csv_File;

	// Defining the filename
	std::string filename;
	if (this->Analysis_data.BG_removed == 0) {
		filename = this->outfile_name + "Raw" + ".csv";
		//std::cout << this->outfile_name << std::endl;
	}
	else
	{
		filename = this->outfile_name + "BGR" + ".csv";
	}

	// Initialize file if not initialized
	if (!std::filesystem::exists(filename)) {

		// Open stream file
		Csv_File.open(filename);

		// Print header
		Csv_File << "File Name" << "," << "exposure" << "," << " SOCF"  << std::endl;
		Csv_File << "," << "," << "Min" << "," << "Max" << "," << "Avg" << std::endl;

	}
	else {
		// File stream object declaration
		Csv_File.open(filename, std::ios_base::app);
	}

	

	// output file name and exposure
	Csv_File << ParentFolderName << ",";
	Csv_File << this->Analysis_data.exposure_value << ",";

	double Frame_Resolution = Thorlabs_Frame_Resolution;

	// For Spatial correlations
	double avg_correlation, sum_correlation = 0;
	for (int i = 0; i < this->correlation_array.size(); i++) {
		sum_correlation += this->correlation_array[i];
	}
	Csv_File << *min_element(this->correlation_array.begin(), this->correlation_array.end()) << ",";
	Csv_File << *max_element(this->correlation_array.begin(), this->correlation_array.end()) << ",";
	avg_correlation = sum_correlation / (Frame_Resolution);
	Csv_File << avg_correlation << std::endl;

	// Close file 
	Csv_File.close();

}



void ACF::Output() {};

void ACF::Para_Calculation() {

	// Specifying the reference pixel to be treated.
	this->Reference_Pixel = Ref_Pixel;

#ifdef lag

	// Output the ref_pixel first
	// Specifying the pixel to be treated.
	this->Pixel_number = Ref_Pixel;

	// Applying the calculation on the relevent pixel.
	// Applying this function adds one correlation value to the correlation array in the object.
	// The position of the correlation value is the same as the pixel number.
	this->Calculation();

	for (unsigned int Pixel_num = Thorlabs_Frame_Resolution / 10+1; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num += Thorlabs_Frame_Resolution / 10) {
		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();
	}

#else 
	for (unsigned int Pixel_num = 0; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num++) {
		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();

	}
#endif
}

void ACF::Calculation() {
#ifdef lag
	Calculation_v2();
#else
	Calculation_v1();
#endif lag
}

void ACF::Calculation_v1() {

	// Following on the ACF equation shows in progress report 6. 

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Var, Pix2_Var, Pix1_STD, Pix2_STD, Correlation_value, available_frames = Available_Frames, Acf_sum = 0;
	double Pix1_temp = 0, Pix2_temp = 0;

	// Find the average
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix1_temp += this->Analysis_data.data_set[iter][this->Reference_Pixel];
		Pix2_temp += this->Analysis_data.data_set[iter][this->Pixel_number];

	}
	Pix1_Avg = Pix1_temp / ((available_frames));
	Pix2_Avg = Pix2_temp / ((available_frames));

	Pix1_temp = 0;
	Pix2_temp = 0;
	// Find the variance, Standrad deviation and ACF
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix1_temp += pow(this->Analysis_data.data_set[iter][this->Reference_Pixel]- Pix1_Avg, 2);
		Pix2_temp += pow(this->Analysis_data.data_set[iter][this->Pixel_number] - Pix2_Avg,2);
		Acf_sum += (this->Analysis_data.data_set[iter][this->Reference_Pixel] - Pix1_Avg)*(this->Analysis_data.data_set[iter][this->Pixel_number] - Pix2_Avg);
	}
	Pix1_Var = Pix1_temp / ((available_frames));
	Pix2_Var = Pix2_temp / ((available_frames));
	Pix1_STD = pow(Pix1_Var, 0.5);
	Pix2_STD = pow(Pix2_Var, 0.5);


	// Calculate ACF
	Correlation_value = (Acf_sum / available_frames) / (Pix1_STD * Pix2_STD);

	//if (this->Pixel_number == this->Reference_Pixel)
	//{
	//	std::cout << Correlation_value << std::endl;
	//}

	// Make sure there are no undefined points
	if (std::to_string(Correlation_value) == "inf") {
		std::cout << "ref : " << this->Reference_Pixel << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	}


	this->correlation_array[this->Pixel_number] = Correlation_value;

	//std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void ACF::Calculation_v2() {

	// Following on the ACF equation shows in progress report 6. 

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Var, Pix2_Var, Pix1_STD, Pix2_STD, Correlation_value, available_frames = Available_Frames, Acf_sum = 0;
	double Pix1_temp = 0, Pix2_temp = 0;

	// Find the average
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1-max_lag; iter++) {
		Pix1_temp += this->Analysis_data.data_set[iter][this->Reference_Pixel];
		Pix2_temp += this->Analysis_data.data_set[iter+this->lag_value][this->Pixel_number];

	}
	Pix1_Avg = Pix1_temp / ((available_frames));
	Pix2_Avg = Pix2_temp / ((available_frames));

	Pix1_temp = 0;
	Pix2_temp = 0;
	// Find the variance, Standrad deviation and ACF
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1-max_lag; iter++) {
		Pix1_temp += pow(this->Analysis_data.data_set[iter][this->Reference_Pixel] - Pix1_Avg, 2);
		Pix2_temp += pow(this->Analysis_data.data_set[iter + this->lag_value][this->Pixel_number] - Pix2_Avg, 2);
		Acf_sum += (this->Analysis_data.data_set[iter][this->Reference_Pixel] - Pix1_Avg) * (this->Analysis_data.data_set[iter + this->lag_value][this->Pixel_number] - Pix2_Avg);
	}
	Pix1_Var = Pix1_temp / ((available_frames));
	Pix2_Var = Pix2_temp / ((available_frames));
	Pix1_STD = pow(Pix1_Var, 0.5);
	Pix2_STD = pow(Pix2_Var, 0.5);


	// Calculate ACF
	Correlation_value = (Acf_sum / available_frames) / (Pix1_STD * Pix2_STD);

	//if (this->Pixel_number == this->Reference_Pixel)
	//{
	//	std::cout << Correlation_value << std::endl;
	//}

	// Make sure there are no undefined points
	if (std::to_string(Correlation_value) == "inf") {
		std::cout << "ref : " << this->Reference_Pixel << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	}

	//if (this ->Pixel_number == this->Reference_Pixel) correlation_array[0] = Correlation_value;
	else this->correlation_array[this->Pixel_number] = Correlation_value;

	//std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void MIF::Para_Calculation() {

	// Specifying the reference pixel to be treated.
	this->Reference_Pixel = Ref_Pixel;

#ifdef lag
	// Output the ref_pixel first
	// Specifying the pixel to be treated.
	this->Pixel_number = Ref_Pixel;

	// Applying the calculation on the relevent pixel.
	// Applying this function adds one correlation value to the correlation array in the object.
	// The position of the correlation value is the same as the pixel number.
	this->Calculation();

	for (unsigned int Pixel_num = Thorlabs_Frame_Resolution/10+1; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num+= Thorlabs_Frame_Resolution / 10) {
		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();
	}
#else 
	for (unsigned int Pixel_num = 0; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num++) {
		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();

	}
#endif lag


}

void MIF::Calculation() {
#ifdef lag
	this->Calculation_v3();
#else
	this->Calculation_v2();
#endif lag
}

void MIF::Calculation_v1() {

	// Following the MI calculation in progress report 6

	// Temp variable instantiasion
	double Correlation_value=0, available_frames = Available_Frames, MIf_sum = 0, Pix1_temp = 0, Pix2_temp = 0;

	// Create the objects holding the distributions
	Struc_pixel* Pix1_Dist = new Struc_pixel(this->Analysis_data.data_set, this->Reference_Pixel);
	Struc_pixel* Pix2_Dist = new Struc_pixel(this->Analysis_data.data_set, this->Pixel_number);
	Struc_2pixel* Pix12_Dist = new Struc_2pixel(this->Analysis_data.data_set, this->Reference_Pixel, this->Pixel_number);

	// Calculating Shannons entropies
	double H_1, H_2, H_12;
	this->H_Calculation(*Pix1_Dist, H_1);
	this->H_Calculation(*Pix2_Dist, H_2);
	this->H_Calculation(*Pix12_Dist, H_12);

	//if (this->Pixel_number == this->Reference_Pixel)
	//{
	//	std::cout << Correlation_value << std::endl;
	//}

	// Make sure there are no undefined points
	if (std::to_string(Correlation_value) == "inf") {
		std::cout << "ref : " << this->Reference_Pixel << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
	}

	// Calculate and store the MI
	this->correlation_array[this->Pixel_number] = H_1 + H_2 - H_12;

	// Clean Resources 
	delete Pix1_Dist;
	delete Pix2_Dist;
	delete Pix12_Dist;

}

void MIF::Calculation_v2() {

	// Initialize the needed variables
	Pixel_Set_t Pix1_data, Pix2_data;

	// Collect the needed distributions
	Struc_pixel::Pixel_data_extractor(this->Analysis_data.data_set, Pix1_data, this->Reference_Pixel);
	Struc_pixel::Pixel_data_extractor(this->Analysis_data.data_set, Pix2_data, this->Pixel_number);

	int N = Pix1_data.size();
	double MI_value = 0;

	std::unordered_set<int> unique_U(Pix1_data.begin(), Pix1_data.end());
	std::unordered_set<int> unique_V(Pix2_data.begin(), Pix2_data.end());

	for (int i : unique_U) {
		for (int j : unique_V) {
			// Calculate |U_i ∩ V_j|
			int U_i_V_j = 0;
			for (int k = 0; k < N; k++) {
				if (Pix1_data[k] == i && Pix2_data[k] == j) {
					U_i_V_j++;
				}
			}

			// Calculate |U_i|
			int U_i = std::count(Pix1_data.begin(), Pix1_data.end(), i);

			// Calculate |V_j|
			int V_j = std::count(Pix2_data.begin(), Pix2_data.end(), j);

			// Calculate mutual information
			if (U_i_V_j > 0) {
				MI_value += (double)U_i_V_j / N * std::log((double)N * U_i_V_j / (U_i * V_j));
			}
		}
	}

	// Calculate and store the MI
	this->correlation_array[this->Pixel_number] = MI_value;

}

void MIF::Calculation_v3() {

	// Initialize the needed variables
	Pixel_Set_t Pix1_data, Pix2_data;

	// Collect the needed distributions
	Struc_pixel::Pixel_data_extractor(this->Analysis_data.data_set, Pix1_data, this->Reference_Pixel);
	Struc_pixel::Pixel_data_extractor(this->Analysis_data.data_set, Pix2_data, this->Pixel_number);

	// Slice the data sets to only include the needed lag
	Lag_Pixel_Set_t Lag_pix1_data, Lag_pix2_data;
	for (int i = 0; i < Lag_pix1_data.size(); i++) Lag_pix1_data[i] = Pix1_data[i];
	for (int j = 0; j < Lag_pix2_data.size(); j++) Lag_pix1_data[j] = Pix2_data[j + this->lag_value];

	int N = Pix1_data.size();
	double MI_value = 0;

	std::unordered_set<int> unique_U(Lag_pix1_data.begin(), Lag_pix1_data.end());
	std::unordered_set<int> unique_V(Lag_pix2_data.begin(), Lag_pix2_data.end());

	for (int i : unique_U) {
		for (int j : unique_V) {
			// Calculate |U_i ∩ V_j|
			int U_i_V_j = 0;
			for (int k = 0; k < N; k++) {
				if (Lag_pix1_data[k] == i && Lag_pix2_data[k] == j) {
					U_i_V_j++;
				}
			}

			// Calculate |U_i|
			int U_i = std::count(Lag_pix1_data.begin(), Lag_pix1_data.end(), i);

			// Calculate |V_j|
			int V_j = std::count(Lag_pix2_data.begin(), Lag_pix2_data.end(), j);

			// Calculate mutual information
			if (U_i_V_j > 0) {
				MI_value += (double)U_i_V_j / N * std::log((double)N * U_i_V_j / (U_i * V_j));
			}
		}
	}
	double Correlation_value = MI_value;
	// Calculate and store the MI
	if (this->Pixel_number == this->Reference_Pixel) correlation_array[0] = Correlation_value;
	else this->correlation_array[this->Pixel_number] = Correlation_value;
}


// Calculate shannon entropy for a Struc_pixel object
void MIF::H_Calculation(Struc_pixel& Distribution, double& H) {
	for (auto value : Distribution.Distribution)
		if (value.second != 0) 
			H -= value.second * std::log2(value.second);
}

// Calculate shannon entropy for a Struc_2pixel object
void MIF::H_Calculation(Struc_2pixel& Distribution, double& H) {
	for (auto value : Distribution.Distribution)
		if (value.second != 0) 
			H -= value.second * std::log2(value.second);
}

void MIF::Output() {};

void MEF::Calculation() {

	// Following on the SOCF equation shows in progress report 6. 

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Var, Pix2_Var, Pix1_STD, Pix2_STD, Correlation_value, available_frames = Available_Frames, Acf_sum = 0;
	double Pix1_temp = 0, Pix2_temp = 0;

	// Find the average
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix2_temp += this->Analysis_data.data_set[iter][this->Pixel_number];

	}
	Pix1_Avg = Pix1_temp / ((available_frames));
	Pix2_Avg = Pix2_temp / ((available_frames));

	Pix1_temp = 0;
	Pix2_temp = 0;
	// Find the variance, Standrad deviation and ACF
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix2_temp += pow(this->Analysis_data.data_set[iter][this->Pixel_number] - Pix2_Avg, 2);
	}
	Pix1_Var = Pix1_temp / ((available_frames));
	Pix2_Var = Pix2_temp / ((available_frames));
	Pix1_STD = pow(Pix1_Var, 1 / 2);
	Pix2_STD = pow(Pix2_Var, 1 / 2);

	// Calculate ACF
	Correlation_value = (Acf_sum / available_frames) / (Pix1_STD * Pix2_STD);


	// Make sure there are no undefined points
	if (std::to_string(Correlation_value) == "inf") {
		std::cout << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	}


	this->correlation_array[this->Pixel_number] = Correlation_value;

	//std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void MEF::Output() {};

void SEF::Calculation() {

	// Following on the SOCF equation shows in progress report 6. 

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Var, Pix2_Var, Pix1_STD, Pix2_STD, Correlation_value, available_frames = Available_Frames, Acf_sum = 0;
	double Pix1_temp = 0, Pix2_temp = 0;

	// Find the average
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix2_temp += this->Analysis_data.data_set[iter][this->Pixel_number];

	}
	Pix1_Avg = Pix1_temp / ((available_frames));
	Pix2_Avg = Pix2_temp / ((available_frames));

	Pix1_temp = 0;
	Pix2_temp = 0;
	// Find the variance, Standrad deviation and ACF
	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix2_temp += pow(this->Analysis_data.data_set[iter][this->Pixel_number] - Pix2_Avg, 2);
	}
	Pix1_Var = Pix1_temp / ((available_frames));
	Pix2_Var = Pix2_temp / ((available_frames));
	Pix1_STD = pow(Pix1_Var, 1 / 2);
	Pix2_STD = pow(Pix2_Var, 1 / 2);

	// Calculate ACF
	Correlation_value = (Acf_sum / available_frames) / (Pix1_STD * Pix2_STD);


	// Make sure there are no undefined points
	if (std::to_string(Correlation_value) == "inf") {
		std::cout << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	}


	this->correlation_array[this->Pixel_number] = Correlation_value;

	//std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void SEF::Output() {};

void Temporal::Calculation() {

	// We start by studying the second correlation function value for pixel 0 and itself over the 200 frames.

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Pix2_Avg, Correlation_value, available_frames = Available_Frames, Pix1_Pix1_Avg;
	long Pix1_Sum = 0, Pix2_Sum = 0, Pix1_Pix2_Sum = 0, Pix1_Pix1_Sum = 0;

	for (int iter = 0; iter < this->Zeroth_Analysis_Data.data_set.size() - 1; iter++) {
		Pix1_Sum += this->Zeroth_Analysis_Data.data_set[iter][this->Pixel_number];
		Pix2_Sum += this->Analysis_data.data_set[iter][this->Pixel_number];
		Pix1_Pix2_Sum += this->Zeroth_Analysis_Data.data_set[iter][this->Pixel_number] * this->Analysis_data.data_set[iter][this->Pixel_number];
		Pix1_Pix1_Sum += this->Zeroth_Analysis_Data.data_set[iter][this->Pixel_number] * this->Zeroth_Analysis_Data.data_set[iter][this->Pixel_number];
	}
	Pix1_Avg = Pix1_Sum / available_frames;
	Pix2_Avg = Pix2_Sum / available_frames;
	Pix1_Pix2_Avg = Pix1_Pix2_Sum / available_frames;
	Pix1_Pix1_Avg = Pix1_Pix1_Sum / available_frames;

	Correlation_value = Pix1_Pix2_Avg / (Pix1_Avg * Pix2_Avg);
	//std::cout << Pixel_num << "    " << Correlation_value << std::endl;
	this->correlation_array[this->Pixel_number] = Correlation_value;
}

void Temporal::Calculation_v2(Data& All_Frames0, Data& All_Framesx, Correlations_t& Correlations, unsigned int Pixel_num) {

	// We start by studying the second correlation function value for pixel 0 and itself over the 200 frames.

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Pix2_Avg, Correlation_value, available_frames = Available_Frames, Pix1_Pix1_Avg;
	long Pix1_Sum = 0, Pix2_Sum = 0, Pix1_Pix2_Sum = 0, Pix1_Pix1_Sum = 0;

	for (int iter = 0; iter < All_Frames0.data_set.size() - 1; iter++) {
		Pix1_Sum += All_Frames0.data_set[iter][Pixel_num];
		Pix2_Sum += All_Framesx.data_set[iter][Pixel_num];
		Pix1_Pix2_Sum += All_Frames0.data_set[iter][Pixel_num] * All_Framesx.data_set[iter][Pixel_num];
		Pix1_Pix1_Sum += All_Frames0.data_set[iter][Pixel_num] * All_Frames0.data_set[iter][Pixel_num];
	}
	Pix1_Avg = Pix1_Sum / available_frames;
	Pix2_Avg = Pix2_Sum / available_frames;
	Pix1_Pix2_Avg = Pix1_Pix2_Sum / available_frames;
	Pix1_Pix1_Avg = Pix1_Pix1_Sum / available_frames;

	Correlation_value = Pix1_Pix2_Avg / (Pix1_Avg * Pix2_Avg);
	//std::cout << Pixel_num << "    " << Correlation_value << std::endl;
	Correlations[Pixel_num] = Correlation_value;
}

void Temporal::Calculation_v1() {

	// We start by studying the second correlation function value for pixel 0 and itself over the 200 frames.

	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Pix2_Avg, Correlation_value, available_frames = Available_Frames;
	long Pix1_Sum = 0, Pix2_Sum = 0, Pix1_Pix2_Sum = 0;

	for (unsigned long iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		unsigned long iter1 = iter + 1;
		Pix1_Sum += this->Analysis_data.data_set[iter][this->Pixel_number];
		Pix2_Sum += this->Analysis_data.data_set[iter1][this->Pixel_number];
		Pix1_Pix2_Sum += this->Analysis_data.data_set[iter][this->Pixel_number] * this->Analysis_data.data_set[iter1][this->Pixel_number];

	}
	Pix1_Avg = Pix1_Sum / available_frames;
	Pix2_Avg = Pix2_Sum / available_frames;
	Pix1_Pix2_Avg = Pix1_Pix2_Sum / available_frames;

	Correlation_value = Pix1_Pix2_Avg / (Pix1_Avg * Pix2_Avg);
	//	std::cout << Pixel_num << "    " << Correlation_value << std::endl;
	this->correlation_array[this->Pixel_number] = Correlation_value;
}

void Temporal::Para_Calculation() {

	// Finding the correlations of all pixels in time.
	for (unsigned int Pixel_num = 0; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num++) {

		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();
	}
}

void Spatial::Calculation() {
	Calculation_V2();
}

void Spatial::Calculation_V1() {
	
	 // We start by studying the second correlation function value for pixel refPixel and other pixels over 200 frames.

	 // Temp variable instantiasion
	 double Pix1_Avg, Pix2_Avg, Pix1_Pix2_Avg, Correlation_value, available_frames = Available_Frames;
	 long Pix1_Sum = 0, Pix2_Sum = 0, Pix1_Pix2_Sum = 0;

	 for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		 Pix1_Sum += this->Analysis_data.data_set[iter][this->Reference_Pixel];
		 Pix2_Sum += this->Analysis_data.data_set[iter][this->Pixel_number];
		 Pix1_Pix2_Sum += this->Analysis_data.data_set[iter][this->Reference_Pixel] * this->Analysis_data.data_set[iter][this->Pixel_number];

	 }
	 Pix1_Avg = Pix1_Sum / available_frames;
	 Pix2_Avg = Pix2_Sum / available_frames;
	 Pix1_Pix2_Avg = Pix1_Pix2_Sum / available_frames;

	 Correlation_value = Pix1_Pix2_Avg / (Pix1_Avg * Pix2_Avg);

	 //if(Pix1_Pix2_Avg == 0 || Pix1_Avg ==0 || Pix2_Avg ==0) std::cout << refPixel << ":" << Pix1_Avg << "   " << Pixel_num << ":" << Pix2_Avg << "   " << Pix1_Pix2_Avg  << "   " << Correlation_value << std::endl;

	 if (std::to_string(Correlation_value) == "inf") {
		 std::cout << "ref : " << this->Reference_Pixel << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		 std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	 }

	 this->correlation_array[this->Pixel_number] = Correlation_value;

	 //std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void Spatial::Calculation_V2() {
	
	// Following on the SOCF equation shows in progress report 6. 
	
	// Temp variable instantiasion
	double Pix1_Avg, Pix2_Avg, Pix1_Pix2_Avg, Correlation_value, available_frames = Available_Frames;
	double Pix1_Sum = 0, Pix2_Sum = 0, Pix1_Pix2_Sum = 0;

	for (int iter = 0; iter < this->Analysis_data.data_set.size() - 1; iter++) {
		Pix1_Sum +=  this->Analysis_data.data_set[iter][this->Reference_Pixel];
		Pix2_Sum +=  this->Analysis_data.data_set[iter][this->Pixel_number];
		Pix1_Pix2_Sum += this->Analysis_data.data_set[iter][this->Reference_Pixel] * this->Analysis_data.data_set[iter][this->Pixel_number];

	}
	Pix1_Avg = Pix1_Sum / ((available_frames));
	Pix2_Avg = Pix2_Sum / ((available_frames));
	Pix1_Pix2_Avg = Pix1_Pix2_Sum / ((available_frames));

	Correlation_value = Pix1_Pix2_Avg / (Pix1_Avg * Pix2_Avg);

	if (this->Pixel_number == this->Reference_Pixel)
	{
		std::cout << Correlation_value << std::endl;
	}

	//if(Pix1_Pix2_Avg == 0 || Pix1_Avg ==0 || Pix2_Avg ==0) std::cout << refPixel << ":" << Pix1_Avg << "   " << Pixel_num << ":" << Pix2_Avg << "   " << Pix1_Pix2_Avg  << "   " << Correlation_value << std::endl;

	if (std::to_string(Correlation_value) == "inf") {
		std::cout << "ref : " << this->Reference_Pixel << " pixelnum: " << this->Pixel_number << " Corr: " << Correlation_value << std::endl;
		std::cout << Pix2_Avg << "   " << Pix1_Avg << std::endl;
	}


	this->correlation_array[this->Pixel_number] = Correlation_value;

	//std::cout << Correlations[Pixel_num]  << "  " << Correlation_value  << std::endl;
}

void Spatial::Para_Calculation() {
	Para_Calculation_V2();
}

void Spatial::Para_Calculation_V1() {

	// Defining temporary array to hold correlation values
	// These arrays are here while we investigate the proces.
	Correlations_t* maxSpatialCorrelation = new Correlations_t; maxSpatialCorrelation->fill(0.0);
	Correlations_t* avgSpatialCorrelation = new Correlations_t; avgSpatialCorrelation->fill(0.0);

	//// Declaring and configuring an array to be used in parallel processing to speed up the computation
	//std::array<unsigned int, Thorlabs_Frame_Resolution> ref_pixel_array;
	//for (int ref_pixel = 0; ref_pixel < Thorlabs_Frame_Resolution; ref_pixel++) {
	//	// Initliazation
	//	ref_pixel_array[ref_pixel] = ref_pixel;
	//}
	//// Loading all frames to the data structure
	//std::for_each(std::execution::par_unseq, ref_pixel_array.begin(), ref_pixel_array.end(), [&](unsigned int& refPixel) {


	for(int refPixel=0; refPixel < Thorlabs_Frame_Resolution; refPixel++){
		
		// Specifying the reference pixel to be treated.
		this->Reference_Pixel = refPixel;

		for (unsigned int Pixel_num = 0; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num++) {
			// Specifying the pixel to be treated.
			this->Pixel_number = Pixel_num;

			// Applying the calculation on the relevent pixel.
			// Applying this function adds one correlation value to the correlation array in the object.
			// The position of the correlation value is the same as the pixel number.
			this->Calculation();
		}

		if (this->max_Para_calculation == 1) {
			//Considering the maximum possible correlation in the frame alone
			maxSpatialCorrelation[0][refPixel] = *max_element(this->correlation_array.begin(), this->correlation_array.end());
		}

		if (this->avg_Para_calculation == 1) {
			// Summing over all values
			for (int value = 0; value < avgSpatialCorrelation->size(); value++) {
				avgSpatialCorrelation[0][value] += this->correlation_array[value];
			}
		}

		// Clearing the existing correlation array to be used again.
		this->correlation_array.fill(0.0);
		}

	if (this->avg_Para_calculation == 1) {
		// Averaging over all values
		for (int value = 0; value < avgSpatialCorrelation->size(); value++) {
			avgSpatialCorrelation[0][value] = avgSpatialCorrelation[0][value] / avgSpatialCorrelation->size();
		}
	}
	
	// Copying the array to be used in the output function. Only the memory from the activated process will be copied.
	if (this->avg_Para_calculation == 1) {
		for (int i =0; i<avgSpatialCorrelation->size(); i++)
		this->correlation_array[i] = avgSpatialCorrelation[0][i];
	}
	else if (this->max_Para_calculation == 1) {
		for (int i = 0; i < maxSpatialCorrelation->size(); i++)
			this->correlation_array[i] = maxSpatialCorrelation[0][i];
	}

	// Deleting the objects for free memory.
	delete maxSpatialCorrelation;
	delete avgSpatialCorrelation;
}

void Spatial::Para_Calculation_V2() {

	// Specifying the reference pixel to be treated.
	this->Reference_Pixel = Ref_Pixel;

	for (unsigned int Pixel_num = 0; Pixel_num < Thorlabs_Frame_Resolution; Pixel_num++) {
		// Specifying the pixel to be treated.
		this->Pixel_number = Pixel_num;

		// Applying the calculation on the relevent pixel.
		// Applying this function adds one correlation value to the correlation array in the object.
		// The position of the correlation value is the same as the pixel number.
		this->Calculation();

	}
}