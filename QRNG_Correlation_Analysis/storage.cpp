#pragma once

#include "storage.h"

void Struc_pixel::data_sorter() {
	// finding the pixel count average over all available frames 
	double Pixel_Avg, Pixel_Sum = 0;
	for (auto pix : this->Pixel_set) {
		Pixel_Sum += pix;
	}
	Pixel_Avg = Pixel_Sum / this->Pixel_set.size();
	
	// initialize the distribution bins
	for (unsigned int bin = 0; bin < this->Distribution.size(); bin++) {
		this->Distribution[bin].first = bin;
		this->Distribution[bin].second = 0;
	}

	// Sort values in struc_frame
	for (int bin = 0; bin < this->Distribution.size(); bin++) {
		for (auto pix : this->Pixel_set) {
			if (this->Distribution[bin].first == pix) this->Distribution[bin].second += 1;
		}
	}

	// Normalize X_axis
	if (this->X_norm) for (int bin = 0; bin < this->Distribution.size(); bin++) this->Distribution[bin].first -= Pixel_Avg;

	// Normalize Y_axis
	if (this->Y_norm) for (int bin = 0; bin < this->Distribution.size(); bin++) this->Distribution[bin].second = this->Distribution[bin].second / Pixel_set.size();

	//for (auto pix : this->Distribution) std::cout << pix.first << " " << pix.second << std::endl;

}

void Struc_pixel::Pixel_data_extractor(All_Frames_t& Data_set, Pixel_Set_t& Pixel_set, int Pixel_number) {
	
	// Extracting Pixel data from the main data set
	for (unsigned int j = 0; j < Data_set.size(); j++) {
		Pixel_set[j]=Data_set[j][Pixel_number];
	}
}

void Struc_2pixel::data_2sorter() {
	// finding the pixel count average over all available frames 
	double Pixel_Avg, Pixel_Sum = 0;
	for (auto pix : this->Two_Pixel_set) {
		Pixel_Sum += pix;
	}
	Pixel_Avg = Pixel_Sum / this->Two_Pixel_set.size();

	// initialize the distribution bins
	for (unsigned int bin = 0; bin < this->Distribution.size(); bin++) {
		this->Distribution[bin].first = bin;
		this->Distribution[bin].second = 0;
	}

	// Sort values in struc_frame
	for (int bin = 0; bin < this->Distribution.size(); bin++) {
		for (auto pix : this->Two_Pixel_set) {
			if (this->Distribution[bin].first == pix) this->Distribution[bin].second += 1;
		}
	}

	// Normalize X_axis
	if (this->X_norm) for (int bin = 0; bin < this->Distribution.size(); bin++) this->Distribution[bin].first -= Pixel_Avg;

	// Normalize Y_axis
	if (this->Y_norm) for (int bin = 0; bin < this->Distribution.size(); bin++) this->Distribution[bin].second = this->Distribution[bin].second / Two_Pixel_set.size();

}

void Struc_2pixel::Two_Pixels_data_extractor(All_Frames_t& Data_set, int Ref_pixel) {
	
	// Copy the data already loaded for the pixel number
	std::copy(this->Pixel_set.begin(), this->Pixel_set.end(), this->Two_Pixel_set.begin());

	// Collect another data set for the Ref_pixel
	this->Pixel_data_extractor(Data_set, this->Pixel_set ,Ref_pixel);

	// Copy the data of the ref pixel
	std::copy(this->Pixel_set.begin(), this->Pixel_set.end(), this->Two_Pixel_set.begin()+this->Pixel_set.size());

}
