#pragma once

#include"processing.cpp"


int main() {

	// Looping all different exposure configurations 
	for (int exposure = StartExposure; exposure <= EndExposure; exposure += ExposureStep) {

#ifdef TemporalCorrelation_op

		// By Declaring this object, all required data are loaded. We can start the analysis right away.
		Temporal* temporal_SOCF = new Temporal(exposure);

		// Finding the correlation of all pixels with them selves in with different exposure times.
		// This function creates an array of SOCF values that can be used in the output.
		temporal_SOCF->Para_Calculation();

		// Outputting the calculated SOCF array. The output has the form of a csv file.
		temporal_SOCF->Output();

		// Deleting the object to free space. 
		delete temporal_SOCF;

#endif TemporalCorrelation_op


#ifdef SpatialCorrelation_op

		// By Declaring this object, all required data are loaded. We can start the analysis right away.
		Spatial* spatial_SOCF = new Spatial(exposure);


		// Applying the relevent calculation for the refrence pixel with all other pixels
		// This function creates an array of SOCF values that can be used in the output.
		spatial_SOCF->Para_Calculation();

		// Outputting the calculated SOCF array. The output has the form of a csv file.
		spatial_SOCF->Raw_out();

		// Deleting the object to free space. 
		delete spatial_SOCF;

#endif SpatialCorrelation_op



#ifdef Stats_op

		// By Declaring this object, all required data are loaded. We can start the analysis right away.
		Stats* stat = new Stats(exposure);

		// Applying the relevent calculation for the refrence pixel with all other pixels
		// This function creates two arrays of statistal data (average and variance) that can be used in the output.
		stat->Calculation();

		// Outputting the calculated SOCF array. The output has the form of a csv file.
		stat->Output();

		// Deleting the object to free space. 
		delete stat;
#endif Stats_op

#ifdef AC


		for (int i = 0; i < max_lag; i += lag_step) {
			// By Declaring this object, all required data are loaded. We can start the analysis right away.
			ACF* ac_function = new ACF(exposure);

			// Track the lag value to iterate
			if (lag_status == true) ac_function->lag_value = i;

			// Applying the relevent calculation for the refrence pixel with all other pixels
			// This function creates two arrays of statistal data (average and variance) that can be used in the output.
			ac_function->Para_Calculation();

			// Outputting the calculated SOCF array. The output has the form of a csv file.
			ac_function->Raw_out();

			// Deleting the object to free space. 
			delete ac_function;

			// iterate once in the lag is not enabled
			if (lag_status == false) break;

	}
#endif AC


#ifdef MI

		for(int i = 0; i < max_lag; i+= lag_step){

			// By Declaring this object, all required data are loaded. We can start the analysis right away.
			MIF* MI_function = new MIF(exposure);

			// Track the lag value to iterate
			if (lag_status == true) MI_function->lag_value = i;

			// Applying the relevent calculation for the refrence pixel with all other pixels
			// This function creates two arrays of statistal data (average and variance) that can be used in the output.
			MI_function->Para_Calculation();

			// Outputting the calculated SOCF array. The output has the form of a csv file.
			MI_function->Raw_out();

			// Deleting the object to free space. 
			delete MI_function;
			
			// iterate once in the lag is not enabled
			if (lag_status == false) break;
		}
#endif MI


#ifdef ME

		// By Declaring this object, all required data are loaded. We can start the analysis right away.
		MEF* ME_function = new MEF(exposure);

		// Applying the relevent calculation for the refrence pixel with all other pixels
		// This function creates two arrays of statistal data (average and variance) that can be used in the output.
		ME_function->Calculation();

		// Outputting the calculated SOCF array. The output has the form of a csv file.
		ME_function->Raw_out();

		// Deleting the object to free space. 
		delete ME_function;
#endif ME


#ifdef SE

		// By Declaring this object, all required data are loaded. We can start the analysis right away.
		SEF* SE_function = new SEF(exposure);

		// Applying the relevent calculation for the refrence pixel with all other pixels
		// This function creates two arrays of statistal data (average and variance) that can be used in the output.
		SE_function->Calculation();

		// Outputting the calculated SOCF array. The output has the form of a csv file.
		SE_function->Raw_out();

		// Deleting the object to free space. 
		delete SE_function;
#endif SE


	}
	return 0;
}

