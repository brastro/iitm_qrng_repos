#pragma once

// Experiment and Code:
// - Make ROI of sensor smaller and read faster frame rate. Try to get the fastest frame rate with the smallest exposure possible. (Done)
// - Figure out pixel arrangement to know the physical distance between pixels (Not needed)
// - Use a random number generator in the analysis (Not needed)
// - Figure out plots to generate with regard to distance and time (Not needed)
// - Optimize loading (Do not load all assets) (Done)



//	Plot:
//	- Make plots for correlation values VS exposure time with gain, LED distance and LED voltage fixed (Done)
//	- Make two plots, one for big image sensor, and one for small ROI. (Done)
//	- Show the relation between the poisson distribution and the correlation in the plot (Done)

// Conclusions:
//	- Smaller exposure lowers the spatial and temporal correlation
//	- No appearant relation between the correlation values and the poisson distribution



// Updates
//	- The current way of temporal correlation 

//
//		In the coming weeks, items in more than one folder will be replaced by shortcuts.Access to filesand folders won't change.Learn more
//		06 / 01 / 2022: I sat down and wrote the list of parameters I need to speceify for the entire QRNG system.
//		Image sensor : Dynamic
//			- Analog Gain
//			- Digital Gain
//			- Exposure
//			- Noise Compensation Techniques
//		LED :
//			-Size / Shape
//		- Power
//			- Divergance
//			- Threshold voltage
//			- Distance from Image Sensor
//		Environment :
//			- Covered by material opaque to the LED wavelength
//			- Doesn't allow dust particles
//		Post Processing :
//			- Proven in Litrature
//			- Fit the parameters to pass all mentioned tests
//		Processing Power :
//			- Suffecient for Aquiring the data from the image sensors
//			- Suffecient for post - processing the data to produce good randomness
//		Testing :
//			- Insuring that the system output randomness isn't affected by malicios actor
//			- Insuring the generated randomness passes NIST STS - Dieharder and TestU01 randomness tests.
//		Data Storage :
//			- Suffecient for data testing
// ################################################################################################
//		07 / 01 / 2022 : I met with professor Anil to ask about what to do in this semester, and talked about the experiments.
//		professor anil asked me to continue working on the experimental setupand also work on how to Identify "Quantum".
//		The idea here is that Poisson statistics comes out of a semi - classiscal treatment of light.where in our case, we
//		want fully QRNG.He asked me to make a measure that insure it is Quantum, and shows the difference between TRNG
//		and QRNG.This can be done by studing the correlation between the outputs of two pixels then developing a
//		correlation function of pixels(This is much similar to the HBT experiment).
//		Experiments to do:
//			-Threshold voltage measurement
// 			- HBT using two LEDs
// 			- Correlation between the output of pixels using a correlation function
// ################################################################################################
// 		11 / 01 / 2022 : I met with professor Anil in the weekly meeting, I showed the correlation functions used
// 		in the optics book by mark fox.After which professor Aniland I proposed that I do 3 types of correlation :
// 			1 - Spatial Pixel Correlation
// 			2 - Temporal Pixel Correlation
// 			3 - Plotting the second order correlation function against the coherence time of the LED used
// ###############################################################################################
// 		13 / 01 / 2022 : I collected(with the help of Musab) two sets of data from Thor's image sensor.
// 			1 - Poissonian distribution set.
// 			2 - Sub_possonian distribution set.
// 		I will work on this data and try to get meanful results from it.
// ################################################################################################
// 		21 / 02 / 2022 : I have wrote the code to make the spatial analysis.The spatial correlation I studied so for
// 		is between pixel 0 and other pixels in the image sensor.So far, all values are close to 1, even for sub_poissonian
// 		distribution.Which shows that the generation process does not really include anti - bunching and is just a coherent
// 		detection at shot noise level.However, more investigationand more rigorous data analysis is needed.
// ################################################################################################
//		09/05/2022 : I have created plots for both temporal and spatial correlations, after a conversation with yazeed, it apears
//		that the way I did the temporal correlation is not necessarly right. What I did was correlate the counts within a specefic 
//		exposure value, thus assuming that exposure is (tau) which is not necessarly true. Now, I will correlate counts of the lowest 
//		exposure value to the next exposure values. This is explained in the equation below.
//		2ndCorrelationFunction = (<P1*P2>)/(<P1>*<P2>) 
//		where <P1> are the avergae counts in exposure value 1, <P2> are the average counts in exposure value 2.
//		In our case, P1 is always exposure 0, and P2 are incremented by 50, up to 1000
//		I found out that the correlation function I coded was actually wrong and corrected it
// ################################################################################################
//		23/06/2022 : I met with professor anil where we discussed report #4. He mentioned that taking avergaes ruins things out
//		as it doesn't show where you have true correlations/anti-correlations. What we care about is to minimize the correlation 
//		between pixels to get a meaningful RNG. Rather than avergaing, we should get the maximum correlation, if that doesn't pass
//		a "threshold", then we can show that the matrix of pixels can be used as an RNG.
//		Another approach is to take the least correlated pixels and use them as pairs and XOR them.
//		Another point that was brought up, is to study the spatial correlation of different exposures over time. I need to measure 
//		the time it takes between every frame and the other. Then show the evolution of spatial correlation. 
//################################################################################################
//		02/07/2022 : Upon further examination, it apears to me that having a sub-Poisson distributtion is bad for a randomness
//		generation using spatial correlation. This is apearant in extreme case of the detection of a specific fock state.
//		Where the variance is 0. This means that there is no randomness to be had at all. The best distribution for randomness
//		generation is an absolute poissonian with fano factor = 1, where there is no correlation between the pixels, and no data 
//		amplification is needed. This should be reflected in the second-order correlation function where the plot is as close to 1 as possible.
//		What I can write a paper about:
//			- Showing that an Image sensor/LED setup doesn't produce a QRNG but a TRNG using the statistical and the  
//			second-order correlation function resutls.
//			- Introducing a methodology of turning any image sensor/LED setup into a TRNG by:
//				- Tuning the image sensor / LED parameters until a poissonian is produced.
//				- Removing dead pixels, as they affect the distribution/
//				- Removing a dark noise of associated to each pixel to cancel any manufacturing or circuit related noise.
//				- Checking the spatial second order correlation function, and setting an acceptable range for the SOCF (betweed 0.95 and 1.05)
//				- Only considering the bits falling within the FWHM of the distribution.
//				- Measuring the entropy of the data using NIST entropy estimators
//				- Amplifing the data using Toeplitz Amplification with a toeplitz factor that is anti-propotional to the
//				entropy per bit produced from NIST entropy estimatros.
//				- Test the output randomness using a suite of tests and modify toeplitz factor as needed.
//			- Other findings related to coherence of modern LEDs dependant of the LED properties.
//		Things to drop:
//			- Raw data investigation, always remove background data, as that produce distributions consistant with the correlation results
//			- Temporal correlation, as the spatial SOCF approach would provide on-the-fly processing solution with equivelant entropy.