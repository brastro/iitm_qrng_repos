#pragma once

#include "storage.h"

// L1 Class that Loads data into a structure, does abstract analysis on it, outputs an analysis file, then discards the data.In this class, I define the data structure as it used across all offspring classes
class Analysis
{
public:

	//The constructor, all processes insude are applied as soon as the object is called
	Analysis(int exposure, std::string filename) {
		outfile_name = filename;
		// Setting the exposure value in the storage object
		Analysis_data.exposure_value = exposure;

		// Loading the data for the relevant exposure.
		Data_loading(Analysis_data, Analysis_data.exposure_value);


	};

	// ***Data loading part***

	// The member that holds the data to be used in the analysis
	Data Analysis_data;

	// an integer that track the lag value
	int lag_value=0;

	// Used for loading frames into the data structure using parallel execution
	void Data_loading(Data& All_Frames, int exposure);

	// Used for loading data from frame files to frame structre
	void Frame_Loading(Frame_t& Frame, std::string& Filename);

	// ***Data analysis part***
	
	// To be ignored
	virtual void Calculation() = 0;


	// The member holding the Correlation data after analysis 
	Correlations_t correlation_array = { 0 };

	// Used for keeping track of the analysis type in the output files
	std::string Mode;

	// ***Data output part***

	// Output file definiton
	std::ofstream outfile;

	// Output file name
	std::string outfile_name = { 0 };
	
	// To be ignored
	virtual void Output() = 0;

	// Output raw data
	void Raw_out();


};

// L2 Class that handles the stats analysis part.
class Stats : public Analysis
{
public:
	Stats(int exposure, std::string filename = "Stats") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// An array holding the average for each frame in the data set
	std::array<double, Available_Frames> Frame_avg_array = {0};

	// And array holding the variance for each frame in the data set
	std::array<double, Available_Frames> Frame_var_Array = {0};

	// Used for Calculating the Average and Variance of the data
	void Calculation();

	// ***Data output part***

	// Used for outputting the Average and Variance of the data 
	void Output();


	// The output file name holder for the data file 
	std::string outfile_name = "Stats_";
};

// L2 Class that handles second order correlation function analysis part.
class SOCF : public Analysis
{
public:
	SOCF(int exposure, std::string filename = "SOCF") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// used to keep track of the pixel in the correlation
	int Pixel_number = { 0 };

	// To be ignored
	virtual void Para_Calculation() = 0;

	// To be ignored
	virtual void Calculation() = 0;

	// ***Data output part***

	// Output summary of SOCF data 
	void Output();


	// outfile name
	std::string outfile_name;
};

// L2 Class that handles Autocorrelation function analysis part.
class ACF : public Analysis
{
public:
	ACF(int exposure, std::string filename = "ACF") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// used to keep track of the pixel in the correlation
	int Pixel_number = { 0 };

	// To be ignored
	void Para_Calculation();

	// To be ignored
	void Calculation();

	void Calculation_v1();

	// For lagged operaions
	void Calculation_v2();

	// The number of the pixel used as a reference 
	int Reference_Pixel = { 0 };

	// ***Data output part***

	// Output summary of ACF data 
	void Output();

	// outfile name
	std::string outfile_name;
};

// L2 Class that handles Mutual Information function analysis part.
class MIF : public Analysis
{
public:
	MIF(int exposure, std::string filename = "MIF") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// used to keep track of the pixel in the correlation
	int Pixel_number = { 0 };

	// To be ignored
	void Para_Calculation();

	// To be ignored
	void Calculation();

	// Calcualting mutual information through shannon entropy
	void Calculation_v1();

	// Calculating mutual information through probabilities 
	void Calculation_v2();

	// Calculating time evolution of mutual information probabilities
	void Calculation_v3(); 

	// Calculate shannon entropy for a Struc_pixel object
	void H_Calculation(Struc_pixel& Distribution, double& H);

	// Calculate shannon entropy for a Struc_2pixel object
	void H_Calculation(Struc_2pixel& Distribution, double& H);

	// The number of the pixel used as a reference 
	int Reference_Pixel = { 0 };

	// ***Data output part***

	// Output summary of ACF data 
	void Output();

	// outfile name
	std::string outfile_name;
};

// L2 Class that handles Shannon Entropy function analysis part.
class SEF : public Analysis
{
public:
	SEF(int exposure, std::string filename = "SEF") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// used to keep track of the pixel in the correlation
	int Pixel_number = { 0 };

	// To be ignored
	void Para_Calculation();

	// Calculate shannon entropy for a Struc_pixel object
	void Calculation();


	// ***Data output part***

	// Output summary of ACF data 
	void Output();

	// outfile name
	std::string outfile_name;
};

// L2 Class that handles Minimum Entropy function analysis part.
class MEF : public Analysis
{
public:
	MEF(int exposure, std::string filename = "MEF") : Analysis(exposure, filename) {
		Mode = filename;
	};

	// ***Data analysis part***

	// used to keep track of the pixel in the correlation
	int Pixel_number = { 0 };

	// To be ignored
	void Para_Calculation();

	// To be ignored
	void Calculation();

	// ***Data output part***

	// Output summary of ACF data 
	void Output();

	// outfile name
	std::string outfile_name;
};

// L3 Class that handles the Temporal correlation part.
class Temporal : public SOCF
{
public:
	Temporal(int exposure) : SOCF(exposure, "Temporal_SOCF_") {
		
		// Loading the data for the zeroth exposure.
		Data_loading(Zeroth_Analysis_Data, Zeroth_Exposure);

	};

	// ***Data loading part***

	// The member that holds the data to be used in the analysis
	Data Zeroth_Analysis_Data;

	// ***Data analysis part***

	// V2, currently used
	// Used for studying the temporal correlation of pixels by correlating the pixels of lowest exposure value to the next exposure values
	void Calculation();

	// V1, deprecated funcion, currently unused
	// Used for studying the temporal correlation of pixels by correlating the pixels within an exposure value
	void Calculation_v1();

	// V2, currently used as Calculation
	// Used for studying the temporal correlation of pixels by correlating the pixels of lowest exposure value to the next exposure values
	void Calculation_v2(Data& Zeroth_Frame, Data& All_Frames, Correlations_t& Correlations, unsigned int Pixel_num);

	// This function handles all upper and pre/post-porcessing calculation 
	void Para_Calculation();

	// ***Data output part***

	// The output file name holder for the data file 
	//std::string outfile_name = "Temporal_SOCF_";

};

// L3 Class that handles the Spatial correlation part.
class Spatial : public SOCF
{
public:
	Spatial(int exposure) : SOCF(exposure, "Spatial_SOCF_") {

	};

	///////////////////// Under Development Varibles ////////////////////////

	// Set this value to 1 if this is the intended process
	bool max_Para_calculation = 1;
	
	// Set this value to 1 if this is the intended process
	bool avg_Para_calculation = 0;

	/////////////////////////////////////////////////////////////////////////

	// The number of the pixel used as a reference the spatial correlation
	int Reference_Pixel = { 0 };

	// Used for studying the spatial correlation of pixels
	void Calculation();

	// This function is the one used in progress report 4 and 5 to generate the needed plot, it is likly to be deprecated.
	void Calculation_V1();

	// This function is under development, it uses the SOCF from progress report 6
	void Calculation_V2();

	// This function is used to extract relevant correlation information from the correlation values. it sits on top of the calculation function.
	void Para_Calculation();

	// This function is used to generate the plots used in progress report 5.
	void Para_Calculation_V1();

	// This function is used to genereate the plots used in progress report 6.
	void Para_Calculation_V2();


	// ***Data output part***

	// The output file name holder for the data file 
	//std::string outfile_name = "Spatial_SOCF_";
};

