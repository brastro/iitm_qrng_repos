#pragma once

// Libraries used  
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <execution>
#include <iomanip>
#include <filesystem>
#include <array>
#include<cmath>
#include<unordered_set>
#include <algorithm>


// Constants
const int Thorlabs_Frame_Resolution = (60 * 60); //720*540-4//1080*1440-21 //80*4 // Actual resolution 1555200, but actual running pixels are 1555179
const int Available_Frames = (300);

// Data type
#define Raw
//#define BGR



// File name and address
#ifdef Raw
	#define ParentFolderName "QRNG_Data/Experiment_8/CP41B/ROI60x60/" 
	#define Output_folder "Raw_Corr/CP41B/"
#endif Raw
#ifdef BGR
	#define ParentFolderName "QRNG_Data/Experiment_8/TLHG/ROI60x60_backgroundremoved/"
	#define Output_folder "Raw_Corr/TLHG/"
#endif BGR
#define FolderName "exp40gain20.000000Voltage1.720000/"
#define FileInitial "IITM_QRNG"
#define FolderEndName "gain20.000000Voltage1.720000/"

// Expousre Parameters
const int StartExposure = 40;
const int EndExposure = 1000;//1000;
const int ExposureStep = 25;
#define Zeroth_Exposure 40

// Operation modes
#define SpatialCorrelation_op
//#define TemporalCorrelation_op
//#define Stats_op
#define AC
#define MI
//#define ME
//#define SE

// Time domain
//#define lag
const bool lag_status = false;
const int max_lag = Available_Frames / 10;
const int lag_step = 1;

// Defining data structure 
using Pixel_t = unsigned short;
using Pixel_Set_t = std::array<Pixel_t, Available_Frames>;
using Two_Pixel_Set_t = std::array<Pixel_t, 2*Available_Frames>;
using Lag_Pixel_Set_t = std::array<Pixel_t, Available_Frames-max_lag>;
using Lag_Two_Pixel_Set_t = std::array<Pixel_t, 2 * (Available_Frames-max_lag)>;
using Frame_t = std::array<Pixel_t, Thorlabs_Frame_Resolution>;
using All_Frames_t = std::array<Frame_t, Available_Frames>;
using Correlations_t = std::array<double, Thorlabs_Frame_Resolution >;
using Stats_t = std::array<double, Available_Frames>;
using Sorted_Dist_t = std::array<std::pair<int, double>,1024>;

// Investigation Constants
const int Ref_Pixel = 1830;		// Default is 0